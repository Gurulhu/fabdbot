import json
import boto3
import requests

session = boto3.session.Session()
aws_client = session.client(
    service_name='secretsmanager'
)

secrets = json.loads(
            aws_client.get_secret_value(
                SecretId = "/fieldrin/discord/secrets"
            )["SecretString"]
        )

def lambda_handler(event, context):
    url = "https://discord.com/api/v8/applications/%s/commands" % secrets["application_id"]

    headers = {
        "Authorization": "Bot %s" % secrets["bot_token"]
    }

    r = requests.post(url, headers=headers, json=event)

    print(r.text)

    return {
        'statusCode': 200,
    }
