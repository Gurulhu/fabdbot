import json
import boto3

from nacl.signing import VerifyKey
from nacl.exceptions import BadSignatureError

session = boto3.session.Session()
aws_client = session.client(
    service_name='secretsmanager'
)

secrets = json.loads(
            aws_client.get_secret_value(
                SecretId = "/fieldrin/discord/secrets"
            )["SecretString"]
        )


def verify_signature(event):
    raw_body = event["body"]
    auth_sig = event['headers']['x-signature-ed25519']
    auth_ts  = event['headers']['x-signature-timestamp']

    message = auth_ts.encode() + raw_body.encode()
    verify_key = VerifyKey(bytes.fromhex(secrets["public_key"]))
    verify_key.verify(message, bytes.fromhex(auth_sig)) # raises an error if unequal

def lambda_handler(event, context):
    #Discord signature verification
    try:
        verify_signature(event)
    except Exception as e:
        print(f"[UNAUTHORIZED] Invalid request signature: {e}")
        return {'statusCode': 401}

    request = json.loads(event["body"])

    #Check if ping-ping:
    if request["type"] == 1:
        print("[PING]")
        return {'statusCode': 200, 'body': json.dumps({"type": 1}) }

    #Do stuff:
    print( request )
    aws_client = session.client(
        service_name='lambda'
    )
    aws_client.invoke(
            FunctionName='fieldrin-discord-response',
            InvocationType='Event',
            Payload=json.dumps({
                    "query" : request["data"]["options"][0]["value"],
                    "token" : request["token"]
                })
        )

    return {
        'statusCode': 200,
        'body': json.dumps({
            'type': 4,
            'data': {
                'tts': False,
                'content': "Oh, yeah, sure, let me see what I can find.",
                'embeds': [],
                'allowed_mentions': []
            }
        })
    }
