extern crate reqwest;

use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use std::collections::HashMap;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(func);
    lambda_runtime::run(func).await?;

    Ok(())
}

async fn func(event: Value, _: Context) -> Result<Value, Error> {
    let query = event["query"].as_str().unwrap_or("Cracked Bauble");
    let token = event["token"].as_str().unwrap();
    let client = reqwest::Client::new();
    let res: serde_json::Value = client
        .get(format!("https://api.fabdb.net/cards?keywords={}", query))
        .send()
        .await?
        .json()
        .await?;

    //#this works: res["data"].as_array().unwrap().len()

    let threshold: u64 = 3; //Normal cards have at most 3 versions (one for each pitch)
    let mut reply = HashMap::new();

    if res["meta"]["total"].as_u64().unwrap().gt(&threshold) {
        reply.insert(
            "content",
            format!(
                "Oh sorry, pardon, I found {} too many cads!",
                res["meta"]["total"]
            ),
        );
    } else {
        reply.insert(
            "content",
            format!(
                "I have a hunch you may be searching for this: {}",
                res["data"][0]["image"]
            ),
        );
    }

    client
        .patch(format!(
            "https://discord.com/api/v8/webhooks/930209904407502868/{}/messages/@original",
            token
        ))
        .json(&reply)
        .send()
        .await?;

    Ok(json!(&reply))
}
