resource "aws_secretsmanager_secret" "fieldrin-discord-secrets" {
  description = "Fieldrin discord secrets"
  name        = "/fieldrin/discord/secrets"
  kms_key_id  = aws_kms_key.fieldrin.key_id
}
