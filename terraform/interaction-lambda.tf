resource "aws_lambda_function" "interactions" {
  filename      = "../lambdas/fieldrin-discord-interaction/lambda_function.zip"
  function_name = "fieldrin-discord-interactions"
  role          = aws_iam_role.interactions.arn

  runtime = "python3.9"
  handler = "lambda_function.lambda_handler"

  layers = [aws_lambda_layer_version.pynacl.arn]

  source_code_hash = filebase64sha256("../lambdas/fieldrin-discord-interaction/lambda_function.zip")
}

resource "aws_cloudwatch_log_group" "interactions" {
  name              = "/aws/lambda/fieldrin-discord-interactions"
  retention_in_days = 14
}

data "aws_iam_policy_document" "interactions-assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "interactions-policy" {
  statement {
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [aws_lambda_function.response.arn]
  }

  statement {
    effect = "Allow"

    actions = [
      "secretsmanager:*",
      "kms:DescribeKey",
      "kms:ListAliases",
      "kms:ListKeys",
      "kms:Decrypt",
    ]

    resources = [aws_kms_key.fieldrin.arn, aws_secretsmanager_secret.fieldrin-discord-secrets.arn]
  }
}

resource "aws_iam_role" "interactions" {
  name                = "FieldrinDiscordInteractionsLambdaRole"
  assume_role_policy  = data.aws_iam_policy_document.interactions-assume.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]
  inline_policy {
    name   = "FieldrinDiscordInteractionsLambdaPolicy"
    policy = data.aws_iam_policy_document.interactions-policy.json
  }
}
