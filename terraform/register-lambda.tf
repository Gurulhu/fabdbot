resource "aws_lambda_function" "register" {
  filename      = "../lambdas/fieldrin-discord-register/lambda_function.zip"
  function_name = "fieldrin-discord-register"
  role          = aws_iam_role.register.arn

  runtime = "python3.9"
  handler = "lambda_function.lambda_handler"

  layers = [aws_lambda_layer_version.requests.arn]

  source_code_hash = filebase64sha256("../lambdas/fieldrin-discord-register/lambda_function.zip")
}

resource "aws_cloudwatch_log_group" "register" {
  name              = "/aws/lambda/fieldrin-discord-register"
  retention_in_days = 14
}

data "aws_iam_policy_document" "register-assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}


data "aws_iam_policy_document" "register-policy" {
  statement {
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [aws_lambda_function.response.arn]
  }

  statement {
    effect = "Allow"

    actions = [
      "secretsmanager:*",
      "kms:DescribeKey",
      "kms:ListAliases",
      "kms:ListKeys",
      "kms:Decrypt",
    ]

    resources = [aws_kms_key.fieldrin.arn, aws_secretsmanager_secret.fieldrin-discord-secrets.arn]
  }
}

resource "aws_iam_role" "register" {
  name                = "FieldrinDiscordRegisterLambdaRole"
  assume_role_policy  = data.aws_iam_policy_document.register-assume.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]
  inline_policy {
    name   = "FieldrinDiscordRegisterLambdaPolicy"
    policy = data.aws_iam_policy_document.register-policy.json
  }
}
