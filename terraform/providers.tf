provider "aws" {
  region = "sa-east-1"

  default_tags {
    tags = {
      Project = "fabdbot"
      Deploy  = "Terraform"
      Repo    = "https://gitlab.com/Gurulhu/fabdbot"
    }
  }
}
