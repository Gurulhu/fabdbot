resource "aws_lambda_function" "response" {
  filename      = "../lambdas/fieldrin-discord-response/rust.zip"
  function_name = "fieldrin-discord-response"
  role          = aws_iam_role.response.arn

  runtime = "provided.al2"
  handler = "hire.me"

  source_code_hash = filebase64sha256("../lambdas/fieldrin-discord-response/rust.zip")
}

resource "aws_cloudwatch_log_group" "response" {
  name              = "/aws/lambda/fieldrin-discord-response"
  retention_in_days = 14
}

data "aws_iam_policy_document" "response-assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "response-policy" {
  statement {
    effect = "Allow"

    actions = [
      "secretsmanager:*",
      "kms:DescribeKey",
      "kms:ListAliases",
      "kms:ListKeys",
      "kms:Decrypt",
    ]

    resources = [aws_kms_key.fieldrin.arn, aws_secretsmanager_secret.fieldrin-discord-secrets.arn]
  }
}

resource "aws_iam_role" "response" {
  name                = "FieldrinDiscordResponseLambdaRole"
  assume_role_policy  = data.aws_iam_policy_document.response-assume.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]
  inline_policy {
    name   = "FieldrinDiscordResponseLambdaPolicy"
    policy = data.aws_iam_policy_document.response-policy.json
  }
}
