resource "aws_lambda_layer_version" "pynacl" {
  filename = "../lambdas/pynacl-layer/python.zip"
  //source_code_hash = ${filebase64sha256("../lambdas/pynacl-layer/python.zip")}

  layer_name          = "pynacl"
  description         = "Cryptographic library based on libsodium"
  compatible_runtimes = ["python3.9"]
}

resource "aws_lambda_layer_version" "requests" {
  filename = "../lambdas/requests-layer/python.zip"
  //source_code_hash = ${filebase64sha256("../lambdas/requests-layer/python.zip")}

  layer_name          = "requests"
  description         = "HTTP/s call library"
  compatible_runtimes = ["python3.9"]
}
